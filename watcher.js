const fs = require('fs')
const path = require('path')

fs.watch(path.join(__dirname, './dist'), (eventType, fileName) => {

  if (eventType === 'change') {

    const fileThatWasChanged = path.join(__dirname, './dist', fileName)
    const content = fs.readFileSync(fileThatWasChanged)
    const parsedData = JSON.parse(content.toString())

    const appendFileFunction = (type) => {
      fs.appendFile(path.join(__dirname, `./audit/${type}.csv`), `\n${parsedData.date},${parsedData.value},${parsedData.name}`, (err) => {

        if (err) console.log(err)

        fs.unlink(path.join(__dirname, './dist', fileName), (err) => {
          if (err) console.log("File deletion error ")
        })
      })
    }

    appendFileFunction(parsedData.type)

  }
})