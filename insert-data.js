const readline = require('readline')
const fs = require('fs')
const path = require('path')

const rl = readline.createInterface({
  output: process.stdout,
  input: process.stdin
})

const validator = (dataName, data) => {

  if (dataName === 'Service') {
    if (data !== 'gas' && data !== 'water') return 'You need to enter "gas" or "water" '
    return undefined
  }

  if (dataName === 'Value') {
    if (data && !/^\d{6}$/.test(data)) return 'You need to enter 6 digits'
    return undefined
  }

  if (dataName === 'Name') {
    if (data && !/^[a-zа-яё]+$/i.test(data)) return 'Only letters need to be entered'
    return undefined
  }
}

function askSync(q, dataName) {
  return new Promise((resolve, reject) => {

    rl.question(q, (data) => {

      const resValid = validator(dataName, data)
      if (resValid) reject(resValid)
      resolve(data)
    })
  })
}

async function asker() {
  try {
    const type = await askSync('What is your service? ', 'Service')
    const name = await askSync('What is your name? ', 'Name')
    const value = await askSync('What data? ', 'Value')

    const date = new Date()
    const newObj = {
      type,
      value,
      name,
      date: `${date.getMonth()}-${date.getDate()}-${date.getFullYear()}`
    }

    fs.writeFile(path.join(__dirname, `./dist/${Date.now()}info.js`), JSON.stringify(newObj), (err) => {
      if (err) {
        console.log("Error adding")
        throw err
      }
      else console.log("\nData received ")
    })

  } catch (err) {
    console.log(err)
  }
  rl.close()
}

asker()