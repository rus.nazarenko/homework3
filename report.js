const fs = require('fs')
const path = require('path')

const printReport = (service) => {
  const fileThatWasChanged = path.join(__dirname, `./audit/${service}.csv`)
  const content = fs.readFileSync(fileThatWasChanged)
  const stringData = content.toString()
  const arrData = stringData.split('\n')
  let sortObj = {}

  arrData.forEach((line, i) => {
    if (i) {
      const values = line.split(',')

      if (!sortObj.hasOwnProperty(values[2])) sortObj[values[2]] = [values]
      else sortObj[values[2]].push(values)
    }
  })

  sortArr = Object.values(sortObj)

  console.log(`\n\n\n============ ${service.toUpperCase()} values ===========`)
  for (let key in sortObj) {
    console.log(`\nUser ${key.toUpperCase()} entered data ${sortObj[key].length} times:`)
    sortObj[key].forEach((item) => {
      console.log(item[0], ' - ', item[1])
    })
  }
}

printReport('gas')
printReport('water')